#!/bin/bash
################################################################################
# start-spark-cluster.sh - start up Spark cluster from within an HPC batch job.
#
# Alexander E. Vapirev, Flemish Supercomputing Center, KU Leuven    Feb, 2020
################################################################################

# === NOTE ===
#
# This script is based on the idea of the original Spark native start-slave.sh.
# Native Spark scripts do not correctly handle the cluster environment
# configuration within this framework and hence won't work if used.
#
# ============

source $DCF_HOME_DIR/sbin/functions.sh

# Reset all environment variables and values to 'none' before assigning them the default values

sc_print "Resetting the default environment variables and parameters, necessary to start the Spark cluster, if previously defined..."

SC_WORKER_INSTANCES=
SC_WORKER_PORT=
SC_WORKER_WEBUI_PORT=

# Manually start here master and all slaves within the established cluster configuration.

# Option strings
SHORT_OPTS="i:p:w:h"
LONG_OPTS="sc-worker-instances:,sc-worker-port:,sc-worker-webui-port:,help"

# read the options
OPTS=$(getopt -s bash --options $SHORT_OPTS --longoptions $LONG_OPTS --name "$0" -- "$@")

if [[ $? != 0 ]] ; then echo "Failed to parse options...exiting." >&2 ; exit_here ; fi

eval set -- "$OPTS"

# extract options and their arguments into variables.
while true ; do
  case "$1" in
    -i | --sc-worker-instances  ) SC_WORKER_INSTANCES="$2"        ; shift 2;;
    -p | --sc-worker-port       ) SC_WORKER_PORT="$2"             ; shift 2;;
    -w | --sc-worker-webui-port ) SC_WORKER_WEBUI_PORT="$2"       ; shift 2;;
    -h | --help                 ) sc_start_print_usage; exit_here ; shift  ;;
    -- ) shift; break;;
    *) echo "$0: error - unrecognized option $2"; exit_here;;
  esac
done


# Start the master node

sc_print "Starting up the Spark cluster..."
sc_print "Starting master..."

SC_MASTER_CLASS="org.apache.spark.deploy.master.Master"
#TODO: if we end up with only one master node then below command will be modified
SC_MASTER_HOST_LIST=`echo ${SC_MASTER_NODE}`

#TODO: Not really necessary to define a loop, might come in handy later when dealing with multiple DFC master nodes (master, namenode, etc...)
i=0
for sc_master_host in ${SC_MASTER_HOST_LIST[@]}; do
    sc_print "Starting all master processes on host ${sc_master_host}..."
    i=$(( 1 + $i ))
    SC_MASTER_NUM=$i
    sc_start_master_instance_cmd="${SPARK_HOME}/sbin/spark-daemon.sh --config ${SPARK_CONF_DIR} start ${SC_MASTER_CLASS} ${SC_MASTER_NUM} --host ${sc_master_host} --port ${SC_MASTERNODE_PORT} --webui-port ${SC_MASTERNODE_WEBUI_PORT}" "$@"
    # Fire up the master daemon over ssh on each master host
    # This way of starting up the master daemon might be useful if the master node
    # is not also the job landing node from which the scripts are being run.
    # Same holds in case of user provided list of hostnames when the master node
    # (first on the list) is not the landing node.
    ssh ${sc_master_host} "${sc_start_master_instance_cmd}" "$@"
done


# Start the worker nodes

# Determine the instances per worker host
if [[ "${SC_WORKER_INSTANCES}" == "" ]]; then
   SC_WORKER_INSTANCES=1
fi
# Export it as it will be needed when/is stopping the cluster
export SC_WORKER_INSTANCES=${SC_WORKER_INSTANCES}
sc_print "SC will fire up ${SC_WORKER_INSTANCES} worker instance(s) on each slave host."

# Determine the worker Web UI port
if [[ "${SC_WORKER_WEBUI_PORT}" == "" ]]; then
  SC_WORKER_WEBUI_PORT=8081
fi
sc_print "Using worker Web UI port number ${SC_WORKER_WEBUI_PORT}..."

SC_WORKER_CLASS="org.apache.spark.deploy.worker.Worker"
SC_SLAVE_HOST_LIST=`cat ${SPARK_CONF_DIR}/slaves`

for sc_slave_host in ${SC_SLAVE_HOST_LIST[@]}; do
    # fire up the worker instances over ssh on each host
    sc_print "Starting all worker instances on host ${sc_slave_host}..."
    for (( i=0; i<${SC_WORKER_INSTANCES}; i++ )); do
        SC_WORKER_NUM=$(( 1 + $i ))

        sc_print "Starting worker instance ${SC_WORKER_NUM} on host ${sc_slave_host}..."

        if [[ "$SC_WORKER_PORT" == "" ]]; then
           SC_PORT_FLAG=
           SC_PORT_NUM=
        else
           SC_PORT_FLAG="--port"
           SC_PORT_NUM=$(( $SC_WORKER_PORT + $SC_WORKER_NUM - 1 ))
           sc_print "Worker instance ${SC_WORKER_NUM} port is ${SC_PORT_NUM} on host ${sc_slave_host}"
        fi
        SC_WEBUI_PORT=$(( $SC_WORKER_WEBUI_PORT + $SC_WORKER_NUM - 1 ))
        sc_print "Worker instance ${SC_WORKER_NUM} Web UI port is ${SC_WEBUI_PORT} on host ${sc_slave_host}"

        sc_start_worker_instance_cmd="${SPARK_HOME}/sbin/spark-daemon.sh --config ${SPARK_CONF_DIR} start ${SC_WORKER_CLASS} ${SC_WORKER_NUM} --webui-port ${SC_WEBUI_PORT} ${SC_PORT_FLAG} ${SC_PORT_NUM} spark://${SPARK_MASTER_HOST}:${SPARK_MASTER_PORT}" "$@"

        # fire up a slave daemon over ssh on the slave host
        ssh ${sc_slave_host} "${sc_start_worker_instance_cmd}" "$@"
    done
done


sc_print ""
sc_print "To stop the Spark cluster use:"
sc_print ""
sc_print '   source $DCF_HOME_DIR/sbin/stop-spark-cluster.sh'
sc_print ""
sc_print "Spark standalone shell can be started interactively with, e.g.:"
sc_print ""
sc_print "   spark-shell --master spark://${SC_MASTER_NODE}:${SC_MASTERNODE_PORT}"
sc_print ""
sc_print "In a standalone mode an interactive pyspark session can be launched with, e.g.:"
sc_print ""
sc_print "   pyspark --master spark://${SC_MASTER_NODE}:${SC_MASTERNODE_PORT}"
sc_print ""
