#!/bin/bash
################################################################################
# functions.sh - a collection of functions common for the DCF bash scripts.
#
# Alexander E. Vapirev, Flemish Supercomputing Center, KU Leuven    Feb, 2020
################################################################################

# Current software version
function dcf_print_version {
    DCF_VERSION=v1.1.2
    echo "${DCF_VERSION}"
}

# Define an exit function to avoid accidentally terminating the HPC job
# if any 'exit' statement is used
function exit_here {
    kill -SIGINT $$
}

# Determine what type of resource manager is being used
function get_resource_manager_type {
    if [[ $PBS_JOBID != "" ]]; then
       RESOURCE_MGR="pbs"
    elif [[ $SLURM_JOBID != "" ]]; then
       RESOURCE_MGR="slurm"
    elif [[ $PE_NODEFILE != "" ]]; then
       RESOURCE_MGR="sge"
    else
       dcf_print "No resource manager found."
    fi
    # override any detected resource manager settings if user hosts list is provided
    if [[ $DCF_USER_HOST_LIST != "" ]]; then
       RESOURCE_MGR="none"
       dcf_print "User provided list of hostnames detected. Resetting the resource manager type to 'none'."
    fi
    if [[ $RESOURCE_MGR == "" ]]; then
       dcf_print "DCF requires either a resource manager, or a user supplied list of hostnames. Neither found. Aborting..."
       dcf_print_usage
       exit_here      
    fi
    dcf_print "Resource manager type is $RESOURCE_MGR..."
}

# This function filters only the unique hostnames from the job hostnames.
# Usually the first host in that list is also the landing node if, e.g.,
# a job is run interactvely. If, however, the landing node is not the first
# in the list then caution must be use when extacting only the unique hosts.
# Piping through 'sort -u' might rearrange the unique host list if the list
# is not by default in advancing order. Piping through 'uniq' might cause
# problems since 'uniq' accounts only for adjacent identical lines/hosts
# in the host list. A workaround is to pipe through " awk '!_[$0]++' ".
# Get the list of all (unique) hostnames
function get_hostnames_list {
    if [[ $RESOURCE_MGR == "pbs" ]]; then
       cat $PBS_NODEFILE | awk '!_[$0]++'
    elif [[ $RESOURCE_MGR == "slurm" ]]; then
       scontrol show hostname $SLURM_NODELIST | awk '!_[$0]++'
    elif [[ $RESOURCE_MGR == "sge" ]]; then
       cat $PE_NODEFILE | awk '!_[$0]++'
    elif [[ $RESOURCE_MGR == "none" ]]; then
       # skip the first two lines containing the number of nodes and cores per node (user input)
       tail -n +3 $DCF_USER_HOST_LIST
    fi
}

# Get job details - number of nodes, number of cores, job ID
function get_job_details {
    if [[ $RESOURCE_MGR == "pbs" ]]; then
       NODES=$PBS_NUM_NODES
       NUMPROCS=$PBS_NP
       JOBID=$PBS_JOBID
    elif [[ $RESOURCE_MGR == "slurm" ]]; then
       NODES=$SLURM_NNODES
       NUMPROCS=$SLURM_NPROCS
       JOBID=$SLURM_JOBID
    elif [[ $RESOURCE_MGR == "sge" ]]; then
       NODES=$NHOSTS
       NUMPROCS=$NSLOTS
       JOBID=$JOB_ID
    elif [[ $RESOURCE_MGR == "none" ]]; then
       # read number of nodes (1st line) and core per node (2nd line) from user input file
       NODES=`sed '1q;d' $DCF_USER_HOST_LIST`
       NUMPROCS=`sed '2q;d' $DCF_USER_HOST_LIST`
       JOBID="no-rsmngr"
    fi
}

# DCF runtime messages
function dcf_print {
    echo "===> Distributed Computing Framework: $@"
}

# Hadoop cluster runtime messages
function hc_print {
    echo "===> Hadoop cluster: $@"
}

# Spark cluster runtime messages
function sc_print {
    echo "===> Spark cluster: $@"
}

# DCF usage
function dcf_print_usage
{
    cat << EOF

        Distributed data Computing cluster Framework $(dcf_print_version)

        Usage: dcf-configure.sh [options]

        Read the user and system environments and set up a common base
        within an HPC job for spawining a Hadoop cluster.
        

        Options: 

        -d, --dcf-home-dir
              The framework home directory.

        -c, --dcf-conf-dir
              The top framework configuration directory.

        -s, --dcf-scratch-dir
              Location of the local framework top scratch directory where the output
              from Hadoop/Spark cluster jobs will be written out.

        -m, --separate-master-host
              Configure (yes) or no (no) one of the hosts as a dedicated master node
              in case of multiple physical host machines.
              (default: yes)

        -l, --user-host-list
              A user provided list of IPs/hostnames in the case of a user owned local cluster.
              If defined, this option overrides any resource manager settings (if RM is present).
              First line containes the number of nodes, second line the number of cores per node,
              then each following line contains a single hostname/IP.
              (default: not set)

        -h, --help
              Show this help message.

        Example:
           source dcf-configure.sh -d <DCF_HOME_DIR> -c <DCF_CONF_DIR> -s <DCF_SCRATCH_DIR> -m no

EOF
}

# HC usage
function hc_print_usage {
    cat << EOF

        Distributed data Computing cluster Framework $(dcf_print_version)

        Usage: hadoop-cluster-configure.sh [options]

        Configure a Hadoop cluster according to the job environment
        and according to user Hadoop configuration settings.
        

        Options: 

        -s, --hc-scratch-dir
              The Hadoop cluster work directory.
              (default: <DCF top scratch dir>/hadoop-cluster-scratch)

        -l, --local-hadoop-conf-dir
              The conf directory of the local Hadoop installation. It will be used
              as a template to configure the master and worker nodes.

        -f, --use-hdfs
              To use (yes) or not (no) HDFS rather than the locally shared FS.
              (default: no)

        -n, --namenode-port
              Port on which the master namenode listens.
              (default: 9000)

        -m, --mapred-framework
              Runtime framework for MapReduce jobs (local, classic, yarn)
              (default: yarn)
     
        -u, --hc-user-conf-dir
              A directory containing user specific configs of the Hadoop cluster.
              (default: not set)

        -h, --help
              Show this help message.

        Example:
           source hadoop-cluster-configure.sh -u <HC_USER_CONFIG_DIR> -f yes

EOF
}

# SC usage
function sc_print_usage {
    cat << EOF

        Distributed data Computing cluster Framework $(dcf_print_version)

        Usage: spark-cluster-configure.sh [options]

        Configure a Spark cluster according to the job environment
        and according to user Spark configuration settings.
        

        Options: 

        -s, --sc-scratch-dir
              The Spark cluster work directory.
              (default: <DCF top scratch dir>/spark-cluster-scratch)

        -l, --local-spark-conf-dir
              The conf directory of the local Spark installation. It will be used
              as a template to configure the master and worker nodes.

        -n, --sc-masternode-port
              Port on which the master node listens.
              (default: 7077)

        -w, --sc-masternode-webui-port
              Port on which the master node listens.
              (default: 8080)

        -u, --sc-user-conf-dir
              A directory containing user specific configs of the Spark cluster.
              (default: not set)

        -y, --sc-use-yarn
              Whether to use Yarn - set it to 'yes' if needed. A hadoop cluster
              must be configured upfront with yarn running for this to work.
              (default: not set)

        -h, --help
              Show this help message.

        Example:
           source spark-cluster-configure.sh -u <SC_USER_CONFIG_DIR> -f yes

EOF
}

# SC start usage
function sc_start_print_usage {
    cat << EOF

        Distributed data Computing cluster Framework $(dcf_print_version)

        Usage: start-spark-cluster.sh [options]

        Start the Spark cluster which has been configured within this framework.
        

        Options: 

        -i, --sc-worker-instances
              The number of worker instances per worker host.
              (default: 1)

        -p, --sc-worker-port
              The base port number for the first worker. If set, subsequent
              workers will increment this number. If unset, Spark will find
              a valid port number, but with no guarantee of a predictable pattern.

        -w, --sc-worker-webui-port
              The base port for the web interface of the first worker.
              Subsequent workers will increment this number.
              (default: 8081)

        -h, --help
              Show this help message.

        Example:
           source start-spark-cluster.sh -i <NUMBER_OF_WORKER_INSTANCES>

EOF
}

# SC stop usage
function sc_stop_print_usage {
    cat << EOF

        Distributed data Computing cluster Framework $(dcf_print_version)

        Usage: stop-spark-cluster.sh [options]

        Stop the Spark cluster which has been configured within this framework.
        

        Options: 

        -h, --help
              Show this help message.

        Example:
           source stop-spark-cluster.sh

EOF
}
