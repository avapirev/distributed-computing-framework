#!/bin/bash
################################################################################
# hadoop-cluster-configure.sh - set up a generic base environement necessary to
# run a Hadoop cluster from within an HPC batch job.
#
# Alexander E. Vapirev, Flemish Supercomputing Center, KU Leuven    Feb, 2020
################################################################################

source $DCF_HOME_DIR/sbin/functions.sh

# Reset all environment variables and values to 'none' before assigning them the default values

hc_print "Resetting the default Hadoop cluster configuration environment variables and parameters if previously defined..."

HC_SCRATCH_DIR=
USE_HDFS=
HC_NAMENODE_PORT=
MAPRED_FRAMEWORK=
HC_USER_CONFIG_DIR=

# Parse command-line options for configuring the Hadoop cluster.

# Option strings
SHORT_OPTS="s:l:f:n:m:u:h"
LONG_OPTS="hc-scratch-dir:,local-hadoop-conf-dir:,use-hdfs:,namenode-port:,mapred-framework:,hc-user-conf-dir:,help"

# read the options
OPTS=$(getopt -s bash --options $SHORT_OPTS --longoptions $LONG_OPTS --name "$0" -- "$@")

if [[ $? != 0 ]] ; then echo "Failed to parse options...exiting." >&2 ; exit_here ; fi

eval set -- "$OPTS"

# extract options and their arguments into variables.
while true ; do
  case "$1" in
    -s | --hc-scratch-dir        ) HC_SCRATCH_DIR="$2"             ; shift 2;;
    -l | --local-hadoop-conf-dir ) LOCAL_HADOOP_CONF_DIR="$2"      ; shift 2;;
    -f | --use-hdfs              ) USE_HDFS="$2"                   ; shift 2;;
    -n | --namenode-port         ) HC_NAMENODE_PORT="$2"           ; shift 2;;
    -m | --mapred-framework      ) MAPRED_FRAMEWORK="$2"           ; shift 2;;
    -u | --hc-user-conf-dir      ) HC_USER_CONFIG_DIR="$2"         ; shift 2;;
    -h | --help                  ) hc_print_usage; exit_here       ; shift  ;;
    -- ) shift; break;;
    *) echo "$0: error - unrecognized option $2"; exit_here;;
  esac
done

# Hadoop installation root dir must be set
if [[ "${HADOOP_HOME}" == "" ]]; then
   hc_print "HADOOP_HOME is not defined." >&2
   hc_print "HADOOP_HOME must be set before configuring a new Hadoop cluster. Aborting." >&2
   hc_print_usage
   exit_here
fi
hc_print "Using HADOOP_HOME=${HADOOP_HOME}"

# Location where the Hadoop cluster config and env files will be stored.
HC_CONF_DIR=${DCF_CONF_DIR}/hadoop-cluster-config
hc_print "Hadoop cluster configuration directory is $HC_CONF_DIR..."

if [[ "${HC_SCRATCH_DIR}" == "" ]]; then
   hc_print "HC scratch directory not specified. Using default settings."
   HC_SCRATCH_DIR=${DCF_SCRATCH_DIR}/hadoop-cluster-scratch
fi
hc_print "Scratch directory for the Hadoop cluster is $HC_SCRATCH_DIR..."
# Remove old duplicate-location scratch if any
if [[ -d "${HC_SCRATCH_DIR}" ]]; then
   hc_print "${HC_SCRATCH_DIR} already exists. Will reset it..."
   rm -rf ${HC_SCRATCH_DIR}
   mkdir ${HC_SCRATCH_DIR}
fi

if [[ "${LOCAL_HADOOP_CONF_DIR}" == "" ]]; then
   hc_print "Local Hadoop configuration directory not specified. Aborting..."
   hc_print_usage
   exit_here
fi
hc_print "Using local Hadoop configuration from ${LOCAL_HADOOP_CONF_DIR}..."

if [[ $HC_NAMENODE_PORT == "" ]]; then
   hc_print "Master namenode port not specified. Using default settings."
   # fs.defaultFS master node port
   HC_NAMENODE_PORT=9000
fi
hc_print "Master namenode port is $HC_NAMENODE_PORT..."

if [[ "$USE_HDFS" == "" || "$USE_HDFS" == "no" ]]; then
   hc_print "File system not specified. Using default settings."
   hc_print "Using local file system..."
   FS_DEFAULTFS="file://"
elif [ "$USE_HDFS" == "yes" ]; then
   hc_print "Using Hadoop file system..."
   FS_DEFAULTFS="hdfs://"
else
   hc_print "The '-H' flag accepts only yes/no as a valid value. Using default settings."
   hc_print "Using local file system..."
   FS_DEFAULTFS="file://"
fi

if [[ "$MAPRED_FRAMEWORK" == "" || "$MAPRED_FRAMEWORK" != "local" || "$MAPRED_FRAMEWORK" != "classic" || "$MAPRED_FRAMEWORK" != "yarn"  ]]; then
   hc_print "Runtime framework for executing MapReduce jobs not specified. Using default settings."
   MAPRED_FRAMEWORK="yarn"
fi
hc_print "Runtime framework for executing MapReduce jobs is $MAPRED_FRAMEWORK..."

if [ "${HC_USER_CONFIG_DIR}" == "" ]; then
   hc_print "User HC configuration not specified. Using default generic settings."
else
   hc_print "User Hadoop cluster configuration is in ${HC_USER_CONFIG_DIR}..."
fi

# Create the cluster config directory and backup old existing ones
if [ -d ${HC_CONF_DIR} ]; then
    i=1
    while [ -d ${HC_CONF_DIR}.bak.${i} ]; do
          let i++
    done
    hc_print "Backing up old config dir to $HC_CONF_DIR.bak.${i}..."
    mv ${HC_CONF_DIR} ${HC_CONF_DIR}.bak.${i}
fi
# Configurations for master and workers are all the same
mkdir -p ${HC_CONF_DIR}


### Begin configuring the Hadoop cluster ###

HC_MASTER_NODE=${DCF_MASTER_NODE}
HC_NUM_WORKERS=${DCF_NUM_WORKERS}

# Minimal default configuration for Hadoop, Yarn, and Map-Reduce.
# Any extra user cluster config input will be added to these.
# TODO: should we keep default parameters explicitely in XMLs and which ones?

# Create a file containing the minimal job-specific Hadoop cluster configuration
# which will be later used to edit the default templates.

HC_CONF_PARAM_FILE=${HC_CONF_DIR}/hadoop-cluster.conf
touch ${HC_CONF_PARAM_FILE}

cat <<EOF > ${HC_CONF_PARAM_FILE}
NODES=$NODES
declare -A HC_Config_Params
# hadoop-env.sh
HC_Config_Params[HADOOP_LOG_DIR]="${HC_SCRATCH_DIR}/logs"
HC_Config_Params[HADOOP_PID_DIR]="${HC_SCRATCH_DIR}/tmp"
# core-site.xml TODO: do we need file:// or hdfs:// for HADOOP_TMP_DIR?????
HC_Config_Params[HC_MASTER_NODE]="$HC_MASTER_NODE"
HC_Config_Params[HADOOP_TMP_DIR]="${HC_SCRATCH_DIR}"
HC_Config_Params[FS_DEFAULTFS]="${FS_DEFAULTFS}${HC_MASTER_NODE}:${HC_NAMENODE_PORT}"
# hdfs-site.xml
HC_Config_Params[DFS_NAMENODE_NAME_DIR]="HADOOP_TMP_DIR/dfs/name"
HC_Config_Params[DFS_DATANODE_DATA_DIR]="HADOOP_TMP_DIR/dfs/data"
HC_Config_Params[DFS_REPLICATION]="$HC_NUM_WORKERS"
HC_Config_Params[DFS_NAME_CHECKPOINT_DIR]="HADOOP_TMP_DIR/dfs/namesecondary"
HC_Config_Params[NFS_DUMP_DIR]="HADOOP_TMP_DIR/.hdfs-nfs"
HC_Config_Params[DFS_DATA_SHARED_DESCR_DIR]="HADOOP_TMP_DIR"
# yarn-site.xml
HC_Config_Params[YARN_RESOURCEMANAGER_HOSTNAME]="$HC_MASTER_NODE"
HC_Config_Params[YARN_NODEMANAGER_LOCAL_DIR]="HADOOP_TMP_DIR/nm-local-dir"
HC_Config_Params[YARN_NODEMANAGER_LOG_DIR]="HADOOP_TMP_DIR/userlogs"
HC_Config_Params[YARN_NODEMANAGER_REMOTE_APP_LOG_DIR]="HADOOP_TMP_DIR/logs"
HC_Config_Params[YARN_SCHEDULER_CONF_DIR]="HADOOP_TMP_DIR/yarn/system/schedconf"
# mapred-site.xml
HC_Config_Params[MAPRED_FRAMEWORK_NAME]="$MAPRED_FRAMEWORK"
HC_Config_Params[MAPRED_CLUSTER_LOCAL_DIR]="HADOOP_TMP_DIR/mapred/local"
HC_Config_Params[YARN_MAPRED_STAGING_DIR]="HADOOP_TMP_DIR/hadoop-yarn/staging"
EOF

# Export the above configuration so that the respective parameters in the
# corresponding Hadoop configuration templates can be adjusted accordingly.

source ${HC_CONF_PARAM_FILE}

# Copy to the configuration dir of the cluster all of the Hadoop configuration
# templates from the existing local Hadoop installation.
# Then modify them according to the system environment and add user settings if any.
   
# copy the generic config templates to the cluster conf dir
scp -r ${LOCAL_HADOOP_CONF_DIR}/* ${HC_CONF_DIR}/

# Copy DCF master and worker hosts lists in HC_CONF_DIR
scp ${DCF_CONF_DIR}/dcf_masters ${HC_CONF_DIR}/masters
scp ${DCF_CONF_DIR}/dcf_workers ${HC_CONF_DIR}/workers


### Define minimal environment specific for the current Hadoop cluster ###

HC_ENV_CONF_FILE=${HC_CONF_DIR}/hadoop-env.sh

# Minimum necessary environment setup in the master environment file
echo "#" >> ${HC_ENV_CONF_FILE}
echo "# Minimal Hadoop cluster specific environement settings" >> $HC_ENV_CONF_FILE
echo "#" >> ${HC_ENV_CONF_FILE}
echo "export JAVA_HOME=${JAVA_HOME}" >> ${HC_ENV_CONF_FILE}
echo "export HADOOP_HOME=${HADOOP_HOME}" >> ${HC_ENV_CONF_FILE}
#
echo "export HADOOP_MAPRED_HOME=${HADOOP_HOME}" >> ${HC_ENV_CONF_FILE}
echo "export HADOOP_COMMON_HOME=${HADOOP_HOME}" >> ${HC_ENV_CONF_FILE}
echo "export HADOOP_HDFS_HOME=${HADOOP_HOME}" >> ${HC_ENV_CONF_FILE}
echo "export YARN_HOME=${HADOOP_HOME}" >> ${HC_ENV_CONF_FILE}
#
echo "export HADOOP_LOG_DIR=${HC_Config_Params[HADOOP_LOG_DIR]}" >> ${HC_ENV_CONF_FILE}
echo "export HADOOP_PID_DIR=${HC_Config_Params[HADOOP_PID_DIR]}" >> ${HC_ENV_CONF_FILE}
echo "export HADOOP_SECURE_PID_DIR=${HC_Config_Params[HADOOP_PID_DIR]}" >> ${HC_ENV_CONF_FILE}
echo "export HADOOP_CONF_DIR=${HC_CONF_DIR}" >> ${HC_ENV_CONF_FILE}
echo "#" >> ${HC_ENV_CONF_FILE}

# Add user Hadoop cluster environement configuration settings if any

# Check if any user env config files exist and if so add their contents
# to the corresponding env files in ${HC_CONF_DIR}.
if [ "${HC_USER_CONFIG_DIR}" != "" ]; then
   # list of original env files
   HCENVFILETYPE="*-env.sh"
   HCENVFILES=`cd ${HC_CONF_DIR} && ls ${HCENVFILETYPE}`
   # list of user env files
   HCUSERENVDIR=${HC_USER_CONFIG_DIR}
   HCUSERENVFILES=`cd ${HCUSERENVDIR} && ls ${HCENVFILETYPE}.user`
   hc_print "Checking for user environement configuration..."
   for hcenvfile in ${HCENVFILES[@]}; do
       for hcuserenvfile in ${HCUSERENVFILES[@]}; do
           if [ "${hcuserenvfile}" == "${hcenvfile}.user" ]; then
              hc_print "Adding user environment configuration to $hcenvfile..."
              cat "${HCUSERENVDIR}/${hcuserenvfile}" >> "${HC_CONF_DIR}/${hcenvfile}"
           fi
       done
   done
fi

### Setup Hadoop cluster configuration parameters ###

# Minimal setup specific for the current Hadoop cluster

# list of default local Hadoop destribution config paramter files
HCPARFILETYPE="*.xml"
HCPARFILES=`cd ${HC_CONF_DIR} && ls ${HCPARFILETYPE}`
# list of default HC config parameter files necessary for minimal run setup
HCDEFAULTPARDIR=${DCF_HOME_DIR}/etc/hc-default-config
HCDEFAULTPARFILES=`cd ${HCDEFAULTPARDIR} && ls ${HCPARFILETYPE}.default`
for hcparfile in ${HCPARFILES[@]}; do
    for hcdefaultparfile in ${HCDEFAULTPARFILES[@]}; do
        if [ "${hcdefaultparfile}" == "${hcparfile}.default" ]; then
           hc_print "Adding default configuration parameters to ${hcparfile}..."
           # insert run specific config in the templates after the first <configuration> tag
           sed -i "/<configuration>/ r ${HCDEFAULTPARDIR}/${hcdefaultparfile}" ${HC_CONF_DIR}/$hcparfile
           for parameter in "${!HC_Config_Params[@]}"; do
               # apply cluster specific parameter values 
               sed -i 's#'${parameter}'#'${HC_Config_Params[${parameter}]}'#g' ${HC_CONF_DIR}/${hcparfile}
           done
        fi
    done
done

# Add user Hadoop cluster parameter configuration settings if any

if [ "${HC_USER_CONFIG_DIR}" != "" ]; then
   # logic - check which *.xml.default match a corresponding template
   # in HC_CONF_DIR and add contents from *.xml.default to *.xml
   # list of default local Hadoop destribution config paramter files
   HCPARFILETYPE="*.xml"
   HCPARFILES=`cd ${HC_CONF_DIR} && ls ${HCPARFILETYPE}`
   # list of user HC config parameter files
   HCUSERPARDIR=${HC_USER_CONFIG_DIR}
   HCUSERPARFILES=`cd ${HCUSERPARDIR} && ls ${HCPARFILETYPE}.user`
   for hcparfile in ${HCPARFILES[@]}; do
       for hcuserparfile in ${HCUSERPARFILES[@]}; do
           if [ "${hcuserparfile}" == "${hcparfile}.user" ]; then
              hc_print "Adding user configuration parameters to ${hcparfile}..."
              # insert run specific config in the templates after the first <configuration> tag
              sed -i "/<configuration>/ r ${HCUSERPARDIR}/${hcuserparfile}" ${HC_CONF_DIR}/${hcparfile}
              for parameter in "${!HC_Config_Params[@]}"; do
                  # apply user defined parameter values
                  sed -i 's#'${parameter}'#'${HC_Config_Params[${parameter}]}'#g' ${HC_CONF_DIR}/${hcparfile}
              done
           fi
       done
   done
fi

# TODO: is sourcing necessary - it starts when daemons are fired up or must be manually?????
# Source Hadoop cluster environment setup.
# Only the main Hadoop environment file will be sourced here by default.
# Any other environments are left for the user to source them manually.

hc_print "Exporting the Hadoop cluster environment..."
source ${HC_ENV_CONF_FILE}

# Message on how to format a new distributed HDFS, start the Hadoop daemons, etc...

HADOOP_HDFS_COMM='$HADOOP_HOME/bin/hdfs'
HADOOP_HDFS_COMM_OPTS="namenode -format"

hc_print ""
hc_print "To format the namenode use (from the master node), e.g.:"
hc_print ""
hc_print "   ${HADOOP_HDFS_COMM} ${HADOOP_HDFS_COMM_OPTS}"
hc_print ""
hc_print "To start the different cluster daemons use, e.g.:"
hc_print ""
hc_print '   $HADOOP_HOME/sbin/start-dfs.sh, start-yarn.sh, etc...'
hc_print ""
hc_print "In a separate-master-host setup it maybe necessary to fire up all daemons with, e.g.:"
hc_print ""
hc_print '   $HADOOP_HOME/sbin/start-all.sh'
hc_print ""
