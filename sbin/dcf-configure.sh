#!/bin/bash
################################################################################
# dcf-configure.sh - read the user and system environments and set up a common
# base within an HPC job for spawining a Hadoop cluster.
#
# Alexander E. Vapirev, Flemish Supercomputing Center, KU Leuven    Feb, 2020
################################################################################

source ${DCF_HOME_DIR}/sbin/functions.sh

# Reset all environment variables and values to 'none' before assigning them the default values

dcf_print "Resetting the default DCF configuration environment variables and parameters if previously defined..."

DCF_SEPARATE_MASTER_HOST=
DCF_USER_HOST_LIST=

# Parse command-line options for configuring DCF.

# Option strings
SHORT_OPTS="d:c:s:m:l:h"
LONG_OPTS="dcf-home-dir:,dcf-conf-dir:,dcf-scratch-dir:,separate-master-host:,user-host-list:,help"

# read the options
OPTS=$(getopt -s bash --options $SHORT_OPTS --longoptions $LONG_OPTS --name "$0" -- "$@")

if [[ $? != 0 ]] ; then echo "Failed to parse options...exiting." >&2 ; exit_here ; fi

eval set -- "$OPTS"

# extract options and their arguments into variables.
while true ; do
  case "$1" in
    -d | --dcf-home-dir         ) DCF_HOME_DIR="$2"                ; shift 2;;
    -c | --dcf-conf-dir         ) DCF_CONF_DIR="$2"                ; shift 2;;
    -s | --dcf-scratch-dir      ) DCF_SCRATCH_DIR="$2"             ; shift 2;;
    -m | --separate-master-host ) DCF_SEPARATE_MASTER_HOST="$2"    ; shift 2;;
    -l | --user-host-list       ) DCF_USER_HOST_LIST="$2"          ; shift 2;;
    -h | --help                 ) dcf_print_usage; exit_here ; shift  ;;
    -- ) shift; break;;
    *) echo "$0: error - unrecognized option $2"; exit_here;;
  esac
done

# Determine the resource manager if any

get_resource_manager_type

# Initialize some general job-specific environment variables

get_job_details

# JAVA_HOME must be set
if [[ "${JAVA_HOME}" == "" ]]; then
   dcf_print "JAVA_HOME is not defined." >&2
   dcf_print "JAVA_HOME must be set before configuring the framework. Aborting." >&2
   dcf_print_usage
   exit_here
fi
dcf_print "Using JAVA_HOME=${JAVA_HOME}"

# The top DCF home dir must be defined
if [[ "${DCF_HOME_DIR}" == "" ]]; then
   dcf_print "You must specify the local DCF home install location. Aborting." >&2
   dcf_print_usage
   exit_here
fi
dcf_print "Using DCF_HOME_DIR=$DCF_HOME_DIR"

# The top DCF configuration dir must be defined
if [[ "${DCF_CONF_DIR}" == "" ]]; then
   dcf_print "You must specify the local configuration location with -c. Aborting." >&2
   dcf_print_usage
   exit_here
fi
dcf_print "Using DCF_CONF_DIR=${DCF_CONF_DIR}"

# The top DCF scratch dir must be defined
if [[ "${DCF_SCRATCH_DIR}" == "" ]]; then
   dcf_print "You must specify the local disk filesystem location with -s. Aborting." >&2
   dcf_print_usage
   exit_here
fi
dcf_print "Using DCF_SCRATCH_DIR=${DCF_SCRATCH_DIR}"

# Create the framework top config directory and backup old existing ones
if [ -d ${DCF_CONF_DIR} ]; then
    i=1
    while [ -d ${DCF_CONF_DIR}.bak.${i} ]; do
          let i++
    done
    dcf_print "Backing up old framework top config dir to $DCF_CONF_DIR.bak.${i}..."
    mv ${DCF_CONF_DIR} ${DCF_CONF_DIR}.bak.${i}
fi
# Create DCF_CONF_DIR
mkdir -p ${DCF_CONF_DIR}

# Separate master node framework setup - yes/no

if [[ $NODES -eq 1 ]]; then
   dcf_print "Master and worker node will be configured to share same host..."
   DCF_SEPARATE_MASTER_HOST="no"
fi

if [[ $NODES -gt 1 ]]; then
   if [[ "$DCF_SEPARATE_MASTER_HOST" == "no" ]]; then
      dcf_print "Master node will not be configured on a separate host..."
   elif [[ "$DCF_SEPARATE_MASTER_HOST" == "yes" ]]; then
      dcf_print "Master node will be configured on a separate host..."
   elif [[ "$DCF_SEPARATE_MASTER_HOST" == "" ]]; then
      dcf_print "Dedicated master node not explicitly set. Using default settings..."
      dcf_print "Master node will be configured on a separate host..."
      DCF_SEPARATE_MASTER_HOST="yes"
   else
      dcf_print "Only 'yes/no' are valid options for a separate master node. Using default settings..."
      dcf_print "Master node will be configured on a separate host..."
      DCF_SEPARATE_MASTER_HOST="yes"
   fi
fi

# Configure master and worker hosts and write them down in files
if [[ $NODES -eq 1 ]]; then
   # If running on a single node then
   # master and worker node are the same machine - create new files
   DCF_MASTER_NODE=$(get_hostnames_list | head -n 1)
   echo $DCF_MASTER_NODE > ${DCF_CONF_DIR}/dcf_masters
   echo $DCF_MASTER_NODE > ${DCF_CONF_DIR}/dcf_workers
   dcf_print "Running on a single host. Designating $DCF_MASTER_NODE as both master and worker"
else
   # If running on multiple hosts then
   if [ "$DCF_SEPARATE_MASTER_HOST" == "no" ]; then
      # The first node in the nodefile will be set as a master - create a new file
      DCF_MASTER_NODE=$(get_hostnames_list | head -n 1)
      dcf_print "Running on multiple hosts. Designating $DCF_MASTER_NODE as master node"
      echo $DCF_MASTER_NODE > ${DCF_CONF_DIR}/dcf_masters

      # All nodes in the nodefile will be workers - create a new file
      get_hostnames_list | tail -n $NODES > ${DCF_CONF_DIR}/dcf_workers
      dcf_print "The following hosts will be workers:"
      while read line; do
            dcf_print "$line"
      done < ${DCF_CONF_DIR}/dcf_workers
   fi
   if [ "$DCF_SEPARATE_MASTER_HOST" == "yes" ]; then
      # The first node in the nodefile will be set as a master - create a new file
      DCF_MASTER_NODE=$(get_hostnames_list | head -n 1)
      dcf_print "Running on multiple hosts. Designating $DCF_MASTER_NODE as master node"
      echo $DCF_MASTER_NODE > ${DCF_CONF_DIR}/dcf_masters

      # The rest of the nodes in the nodefile will be workers - create a new file
      get_hostnames_list | tail -n $(( $NODES - 1 )) > ${DCF_CONF_DIR}/dcf_workers
      dcf_print "The following hosts will be workers:"
      while read line; do
            dcf_print "$line"
      done < ${DCF_CONF_DIR}/dcf_workers
   fi
fi

# Helpful variables to use later - master node name and number of workers

# no-separate-master-host cluster setup
if [ "$DCF_SEPARATE_MASTER_HOST" == "no" ]; then
   # Designating master and worker nodes.
   # Master shares the same host machine with a worker (1st from node list)
   DCF_MASTER_NODE=$(get_hostnames_list | head -n 1)
   DCF_NUM_WORKERS=$NODES
fi

# separate-master-host cluster setup
if [ "$DCF_SEPARATE_MASTER_HOST" == "yes" ]; then
   # Designating master and worker nodes.
   # The first node in the nodefile will be set as a master, the rest as workers
   DCF_MASTER_NODE=$(get_hostnames_list | head -n 1)
   DCF_NUM_WORKERS=$(( $NODES - 1 ))
fi

dcf_print ""
dcf_print "To configure a Hadoop/Spark cluster use, e.g.:"
dcf_print ""
dcf_print '   source $DCF_HOME_DIR/sbin/hadoop-cluster-configure.sh'
dcf_print ""
dcf_print '   source $DCF_HOME_DIR/sbin/spark-cluster-configure.sh'
dcf_print ""
dcf_print "Run the above scripts with '--help' flag for more info."
dcf_print ""
