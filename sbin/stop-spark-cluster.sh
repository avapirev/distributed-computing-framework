#!/bin/bash
################################################################################
# stop-spark-cluster.sh - stop Spark cluster started within an HPC batch job.
#
# Alexander E. Vapirev, Flemish Supercomputing Center, KU Leuven    Feb, 2020
################################################################################

# === NOTE ===
#
# This script is based on the idea of the original Spark native stop-master/slave.sh.
# Native Spark scripts do not correctly handle the cluster environment
# configuration within this framework and hence won't work if used.
#
# ============

source $DCF_HOME_DIR/sbin/functions.sh

# This script will stop the master and all worker instances on each slave host

# Option strings
SHORT_OPTS="h"
LONG_OPTS="help"

# read the options
OPTS=$(getopt -s bash --options $SHORT_OPTS --longoptions $LONG_OPTS --name "$0" -- "$@")

if [[ $? != 0 ]] ; then echo "Failed to parse options...exiting." >&2 ; exit_here ; fi

eval set -- "$OPTS"

# extract options and their arguments into variables.
while true ; do
  case "$1" in
    -h | --help ) sc_stop_print_usage; exit_here ; shift ;;
    -- ) shift; break;;
    *) echo "$0: error - unrecognized option $2"; exit_here;;
  esac
done

# Shut down the Spark cluster
sc_print "Shutting down the Spark cluster..."

# Stop all workers first

SC_WORKER_CLASS="org.apache.spark.deploy.worker.Worker"
SC_SLAVE_HOST_LIST=`cat ${SPARK_CONF_DIR}/slaves`

for sc_slave_host in ${SC_SLAVE_HOST_LIST[@]}; do
    # stop the worker instances over ssh on each host
    sc_print "Stopping all worker instances on host ${sc_slave_host}..."
    for (( i=0; i<$SC_WORKER_INSTANCES; i++ )); do
        SC_WORKER_NUM=$(( 1 + $i ))
        sc_print "Stopping worker instance ${SC_WORKER_NUM} on host ${sc_slave_host}..."
        sc_stop_worker_instance_cmd="${SPARK_HOME}/sbin/spark-daemon.sh --config ${SPARK_CONF_DIR} stop ${SC_WORKER_CLASS} ${SC_WORKER_NUM}" "$@"
        # kill a slave daemon over ssh on the slave host
        ssh ${sc_slave_host} "${sc_stop_worker_instance_cmd}" "$@"
        # make sure the stopping of this process has finished before issuing the next cancelation command
        wait        
    done
done

# Stop the master nodes

sc_print "Stopping the master node(s)..."

SC_MASTER_CLASS="org.apache.spark.deploy.master.Master"
#TODO: if we end up with only one master node then below command will be modified
SC_MASTER_HOST_LIST=`cat ${SPARK_CONF_DIR}/masters`

#TODO: Not really necessary to define a loop, might come in handy later when dealing with multiple DFC master nodes (master, namenode, etc...)
i=0
for sc_master_host in ${SC_MASTER_HOST_LIST[@]}; do
    sc_print "Stopping all master processes on host ${sc_master_host}..."
    i=$(( 1 + $i ))
    SC_MASTER_NUM=$i
    sc_stop_master_instance_cmd="${SPARK_HOME}/sbin/spark-daemon.sh --config ${SPARK_CONF_DIR} stop ${SC_MASTER_CLASS} ${SC_MASTER_NUM}" "$@"
    # kill the master daemon over ssh on each master host
    ssh ${sc_master_host} "${sc_stop_master_instance_cmd}" "$@"
    # make sure the stopping of this process has finished before issuing the next cancelation command
    wait        
done

sc_print ""
sc_print "Done."
sc_print ""
