#!/bin/bash
################################################################################
# spark-cluster-configure.sh - set up a generic base environement necessary to
# run a Spark cluster from within an HPC batch job.
#
# Alexander E. Vapirev, Flemish Supercomputing Center, KU Leuven    Feb, 2020
################################################################################

source $DCF_HOME_DIR/sbin/functions.sh

# Reset all environment variables and values to 'none' before assigning them the default values

sc_print "Resetting the default Spark cluster configuration environment variables and parameters if previously defined..."

SC_SCRATCH_DIR=
SC_MASTERNODE_PORT=
SC_MASTERNODE_WEBUI_PORT=
SC_USER_CONFIG_DIR=
SC_USE_YARN=

# Option strings
SHORT_OPTS="s:l:n:w:u:y:h"
LONG_OPTS="sc-scratch-dir:,local-spark-conf-dir:,sc-masternode-port:,sc-masternode-webui-port:,sc-user-conf-dir:,sc-use-yarn:,help"

# read the options
OPTS=$(getopt -s bash --options $SHORT_OPTS --longoptions $LONG_OPTS --name "$0" -- "$@")

if [[ $? != 0 ]] ; then echo "Failed to parse options...exiting." >&2 ; exit_here ; fi

eval set -- "$OPTS"

# extract options and their arguments into variables.
while true ; do
  case "$1" in
    -s | --sc-scratch-dir             ) SC_SCRATCH_DIR="$2"             ; shift 2;;
    -l | --local-spark-conf-dir       ) LOCAL_SPARK_CONF_DIR="$2"       ; shift 2;;
    -n | --sc-masternode-port         ) SC_MASTERNODE_PORT="$2"         ; shift 2;;
    -w | --sc-masternode-webui-port   ) SC_MASTERNODE_WEBUI_PORT="$2"   ; shift 2;;
    -u | --sc-user-conf-dir           ) SC_USER_CONFIG_DIR="$2"         ; shift 2;;
    -y | --sc-use-yarn                ) SC_USE_YARN="$2"                ; shift 2;;
    -h | --help                       ) sc_print_usage; exit_here       ; shift  ;;
    -- ) shift; break;;
    *) echo "$0: error - unrecognized option $2"; exit_here;;
  esac
done

# Spark installation root dir must be set
if [[ "${SPARK_HOME}" == "" ]]; then
   sc_print "SPARK_HOME is not defined." >&2
   sc_print "SPARK_HOME must be set before configuring a new Spark cluster. Aborting." >&2
   sc_print_usage
   exit_here
fi
sc_print "Using SPARK_HOME=${SPARK_HOME}"

# Location where the Spark cluster config and env files will be stored.
SC_CONF_DIR=${DCF_CONF_DIR}/spark-cluster-config
sc_print "Spark cluster configuration directory is $SC_CONF_DIR..."

if [[ "${SC_SCRATCH_DIR}" == "" ]]; then
   sc_print "SC scratch directory not specified. Using default settings."
   SC_SCRATCH_DIR=${DCF_SCRATCH_DIR}/spark-cluster-scratch
fi
sc_print "Scratch directory for the Spark cluster is $SC_SCRATCH_DIR..."
# Remove old duplicate-location scratch if any
if [[ -d "${SC_SCRATCH_DIR}" ]]; then
   sc_print "${SC_SCRATCH_DIR} already exists. Will reset it..."
   rm -rf ${SC_SCRATCH_DIR}
   mkdir ${SC_SCRATCH_DIR}
fi

if [[ "${LOCAL_SPARK_CONF_DIR}" == "" ]]; then
   sc_print "Local Spark configuration directory not specified. Aborting..."
   sc_print_usage
   exit_here
fi
sc_print "Using local Spark configuration from ${LOCAL_SPARK_CONF_DIR}..."

if [[ "${SC_USE_YARN}" == "yes" ]]; then
   sc_print "Spark will use Yarn..."
fi

if [[ $SC_MASTERNODE_PORT == "" ]]; then
   sc_print "Master node port not specified. Using default settings."
   # default master node port
   SC_MASTERNODE_PORT=7077
fi
sc_print "Master node port is $SC_MASTERNODE_PORT..."

if [ "$SC_MASTERNODE_WEBUI_PORT" = "" ]; then
   sc_print "Master node Web UI port not specified. Using default settings."
   # default master node Web UI port
   SC_MASTERNODE_WEBUI_PORT=8080
fi
sc_print "Master node Web UI port is $SC_MASTERNODE_WEBUI_PORT..."

if [ "${SC_USER_CONFIG_DIR}" == "" ]; then
   sc_print "User SC configuration not specified. Using default generic settings."
else
   sc_print "User Spark cluster configuration is in ${SC_USER_CONFIG_DIR}..."
fi

# Create the cluster config directory and backup old existing ones
if [ -d ${SC_CONF_DIR} ]; then
    i=1
    while [ -d ${SC_CONF_DIR}.bak.${i} ]; do
          let i++
    done
    sc_print "Backing up old config dir to $SC_CONF_DIR.bak.${i}..."
    mv ${SC_CONF_DIR} ${SC_CONF_DIR}.bak.${i}
fi
# Configurations for master and workers are all the same
mkdir -p ${SC_CONF_DIR}


### Begin configuring the Spark cluster ###

# Copy to the configuration dir of the cluster all of the Spark configuration
# templates from the existing local Spark installation.
# Then modify them according to the system environment and add user settings if any.
   
# copy the generic config templates to the cluster conf dir
scp -r ${LOCAL_SPARK_CONF_DIR}/* ${SC_CONF_DIR}/

# Copy DCF master and worker hosts lists in SC_CONF_DIR
# Writing down the master in a separate file is not necessary in Spark - just for bookkeeping
scp ${DCF_CONF_DIR}/dcf_masters ${SC_CONF_DIR}/masters
scp ${DCF_CONF_DIR}/dcf_workers ${SC_CONF_DIR}/slaves


### Define minimal environment specific for the current Spark cluster ###

SC_MASTER_NODE=${DCF_MASTER_NODE}
SC_NUM_WORKERS=${DCF_NUM_WORKERS}

SC_ENV_CONF_FILE=${SC_CONF_DIR}/spark-env.sh
if [[ ! -f "${SC_ENV_CONF_FILE}" ]]; then
   touch ${SC_ENV_CONF_FILE}
   echo '#!/bin/bash' >> ${SC_ENV_CONF_FILE}
fi

# Minimum necessary environment setup in the master environment file
echo "#" >> ${SC_ENV_CONF_FILE}
echo "export JAVA_HOME=${JAVA_HOME}" >> ${SC_ENV_CONF_FILE}
echo "export SPARK_CONF_DIR=${SC_CONF_DIR}" >> ${SC_ENV_CONF_FILE}
echo "export SPARK_MASTER_IP=${SC_MASTER_NODE}" >> ${SC_ENV_CONF_FILE}
echo "export SPARK_MASTER_HOST=${SC_MASTER_NODE}" >> ${SC_ENV_CONF_FILE}
echo "export SPARK_MASTER_PORT=${SC_MASTERNODE_PORT}" >> ${SC_ENV_CONF_FILE}
echo "export SPARK_WORKER_DIR=${SC_SCRATCH_DIR}/work" >> ${SC_ENV_CONF_FILE}
echo "export SPARK_LOCAL_DIRS=${SC_SCRATCH_DIR}/local" >> ${SC_ENV_CONF_FILE}
echo "export SPARK_LOG_DIR=${SC_SCRATCH_DIR}/logs" >> ${SC_ENV_CONF_FILE}
echo "export SPARK_PID_DIR=${SC_SCRATCH_DIR}/tmp" >> ${SC_ENV_CONF_FILE}
if [[ "$SC_USE_YARN" == "yes" ]]; then
   echo "export HADOOP_CONF_DIR=${HC_CONF_DIR}" >> ${SC_ENV_CONF_FILE}
fi
echo "export PATH=$PATH:$SPARK_HOME/bin:$SPARK_HOME/sbin" >> ${SC_ENV_CONF_FILE}
echo "#" >> ${SC_ENV_CONF_FILE}

# Add user Spark cluster environement configuration settings if any

# Edit only spark-env.sh. The rest of the native templates will not be used
# and all user config files are copied to the Spark cluster conf dir.
if [ "${SC_USER_CONFIG_DIR}" != "" ]; then
   # user env file
   SCUSERENVFILE=${SC_USER_CONFIG_DIR}/spark-env.sh.user
   sc_print "Adding user environment configuration to ${SC_ENV_CONF_FILE}..."
   cat "${SCUSERENVFILE}" >> "${SC_ENV_CONF_FILE}"
   # copy any other user conf files that might be in user conf dir
   SCUSERCONFFILES=`cd ${SC_USER_CONFIG_DIR} && ls *.user`
   for scuserconffile in ${SCUSERCONFFILES[@]}; do
       # copy user configuration to SPARK_CONF_DIR
       if [[ "${scuserconffile}" != "spark-env.sh.user" ]]; then
          # copy while cutting out '.user' from filename - sed-like style bash pattern magic
          scp ${SC_USER_CONFIG_DIR}/${scuserconffile} ${SC_CONF_DIR}/${scuserconffile/.user/}
       fi
   done
fi

sc_print "Exporting the Spark cluster environment..."
source ${SC_ENV_CONF_FILE}

# Messages

sc_print ""
sc_print "The configured Spark cluster can be launched with default settings with:"
sc_print ""
sc_print '   source $DCF_HOME_DIR/sbin/start-spark-cluster.sh'
sc_print ""
sc_print "Run the above script with '--help' flag for more info."
sc_print ""

