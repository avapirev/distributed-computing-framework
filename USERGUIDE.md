# Distributed data Computing cluster Framework

# User Guide

## General information

The general idea behind configuring Apache Hadoop and Spark clusters is that the respective software must be loaded and configured on each physical host machine. For personal clusters that may be straightforward. However, that might be a bit awkward within a traditional HPC batch job since each such job generally lands on a completely random set of hosts within the shared HPC cluster. DCF takes into account the batch job environment settings to determine the number and the names of the job-specific hosts and, based on that and also any extra user configuration settings passed on to the framework, configures and exports certain settings necessary for a successful Hadoop/Spark cluster launch.

The core idea behind the framework design is that in a shared cluster environment upon a job start 1) the default user environment is automatically exported on all job hosts, and, 2) all user directories are automatically mounted on all job hosts. That immediately eliminates the need the software and its configuration to be copied and exported to each and every of the physical hosts within that batch job. Thus, a single copy of any Hadoop/Spark software can be either loaded as an evironment module (if the HPC cluster supports that), or the user can put those in any of the available own directories on the cluster.

The scripts generally follow a certain notation pattern for variables and functions: with prefixes DCF_ (or dcf_) for the main DCF scripts, HC_ (or hc_) for Hadoop related scripting, and SC_ (or sc_) for anything Spark.

The DCF directory structure is as follows:

**etc/** - contains some default base configuration settings for Hadoop and Spark.

**examples/** - contains example user configuration settings.

**sbin/** - contains all shell scripts used in the framework.

## Configuration workflow

*General note:*
Due to the fact that many important variables are used and shared among the various scripts it might be advisable to `source` the scripts than executing them, e.g., with either `./` or with `bash`.

The first thing to do is to set up the base framework environment. This is done via the **dcf-configure.sh** script. In case of user specified list of hostnames it might be a good idea if the master node (first node in the hosts list) is the same node from which scripts are being sourced and executed. Then a Hadoop and/or Spark clusters can be configured with respectively **hadoop-cluster-configure.sh** and **spark-cluster-configure.sh**. If the Spark application will use Yarn as a resource manager then a Hadoop cluster must be configured beforehand and Yarn fired up (use of HDFS is optional) and only then a Spark cluster can be configured. The native Apache Hadoop start/stop scripts work fine within an HPC job. However, the respective Spark start/stop shell scripts do not respect framework environment and therefore firing up and stopping the so configured Spark cluster is done via two DCF scripts - **start-spark-cluster.sh** and **stop-spark-cluster.sh**.

## Adding custom user configuration

The framework allows the user to fine tune the clusters by pointing to a directory containing additional configuration files. Each configuration file must have a **.user** extention. Check the **examples/** directory on how to properly name the files and also about example contents for each configuration file type. For Hadoop, the user environment files are attached at the end of the base configuration. For any of the XML configuration files the user parameters are inserted in between the `<configuration>` tags. Thus the user XML config files must not contain their own `<configration>` tags but only properties. For Spark, any user environment setings are attached at the end of the base *spark-env.sh* configuration file. The rest of the user Spark config files are simply copied over to the main config directory.

## Passing parameters

Each bash script comes with a help option `-h/--help`.

## Hadoop configuration

Please refer to the official Apache Hadoop user guide for more info.

## Spark configuration

Please refer to the official Apache Spark user guide for more info.

## Framework configuration options

        Usage: dcf-configure.sh [options]

        Read the user and system environments and set up a common base
        within an HPC job for spawining a Hadoop cluster.
        

        Options: 

        -d, --dcf-home-dir
              The framework home directory.

        -c, --dcf-conf-dir
              The top framework configuration directory.

        -s, --dcf-scratch-dir
              Location of the local framework top scratch directory where the output
              from Hadoop/Spark cluster jobs will be written out.

        -m, --separate-master-host
              Configure (yes) or no (no) one of the hosts as a dedicated master node
              in case of multiple physical host machines.
              (default: yes)

        -l, --user-host-list
              A user provided list of IPs/hostnames in the case of a user owned local cluster.
              If defined, this option overrides any resource manager settings (if RM is present).
              First line containes the number of nodes, second line the number of cores per node,
              then each following line contains a single hostname/IP. The first hostname in
              the list will be designated as master node.
              (default: not set)

        -h, --help
              Show this help message.

        Example:
           source dcf-configure.sh -d <DCF_HOME_DIR> -c <DCF_CONF_DIR> -s <DCF_SCRATCH_DIR> -m no


## Hadoop configuration options

        Usage: hadoop-cluster-configure.sh [options]

        Configure a Hadoop cluster according to the job environment
        and according to user Hadoop configuration settings.
        

        Options: 

        -s, --hc-scratch-dir
              The Hadoop cluster work directory.
              (default: <DCF top scratch dir>/hadoop-cluster-scratch)

        -l, --local-hadoop-conf-dir
              The conf directory of the local Hadoop installation. It will be used
              as a template to configure the master and worker nodes.

        -f, --use-hdfs
              To use (yes) or not (no) HDFS rather than the locally shared FS.
              (default: no)

        -n, --namenode-port
              Port on which the master namenode listens.
              (default: 9000)

        -m, --mapred-framework
              Runtime framework for MapReduce jobs (local, classic, yarn)
              (default: yarn)
     
        -u, --hc-user-conf-dir
              A directory containing user specific configs of the Hadoop cluster.

        -h, --help
              Show this help message.

        Example:
           source hadoop-cluster-configure.sh -u <HC_USER_CONFIG_DIR> -f yes


## Spark configuration options

        Usage: spark-cluster-configure.sh [options]

        Configure a Spark cluster according to the job environment
        and according to user Spark configuration settings.
        

        Options: 

        -s, --sc-scratch-dir
              The Spark cluster work directory.
              (default: <DCF top scratch dir>/spark-cluster-scratch)

        -l, --local-spark-conf-dir
              The conf directory of the local Spark installation. It will be used
              as a template to configure the master and worker nodes.

        -n, --sc-masternode-port
              Port on which the master node listens.
              (default: 7077)

        -w, --sc-masternode-webui-port
              Port on which the master node listens.
              (default: 8080)

        -u, --sc-user-conf-dir
              A directory containing user specific configs of the Spark cluster.
              (default: not set)

        -y, --sc-use-yarn
              Whether to use Yarn - set it to 'yes' if needed. A hadoop cluster
              must be configured upfront with yarn running for this to work.
              (default: not set)

        -h, --help
              Show this help message.

        Example:
           source spark-cluster-configure.sh -u <SC_USER_CONFIG_DIR> -f yes


## Start Spark cluster

        Usage: start-spark-cluster.sh [options]

        Start the Spark cluster which has been configured within this framework.
        

        Options: 

        -i, --sc-worker-instances
              The number of worker instances per worker host.
              (default: 1)

        -p, --sc-worker-port
              The base port number for the first worker. If set, subsequent
              workers will increment this number. If unset, Spark will find
              a valid port number, but with no guarantee of a predictable pattern.

        -w, --sc-worker-webui-port
              The base port for the web interface of the first worker.
              Subsequent workers will increment this number.
              (default: 8081)

        -h, --help
              Show this help message.

        Example:
           source start-spark-cluster.sh -i <NUMBER_OF_WORKER_INSTANCES>


## Stop Spark cluster

        Usage: stop-spark-cluster.sh [options]

        Stop the Spark cluster which has been configured within this framework.
        

        Options: 

        -h, --help
              Show this help message.

        Example:
           source stop-spark-cluster.sh


# Example Hadoop/Spark cluster configuration workflow

(Within an HPC user job)

## Modules and user configuration directories (if any)

``` bash
module load Java Hadoop/3.2.1 Spark/3.0.0

export DCF_HOME_DIR=/path-to/distributed-computing-framework_top_dir
export DCF_CONF_DIR=/path-to-store/framework-configuration
export DCF_SCRATCH_DIR=/path-to-store/framework-output

export JAVA_HOME=/path-to/java-top-dir
export HADOOP_HOME=/path-to/hadoop-installation-top-dir
export SPARK_HOME=/path-to/spark-installation-top-dir
export LOCAL_HADOOP_CONF_DIR=/path-to/hadoop-installation-config-templates (e.g., HADOOP_HOME/etc/hadoop)
export LOCAL_SPARK_CONF_DIR=/path-to/spark-installation-config-templates (e.g., SPARK_HOME/conf)
```

### User directories containing additional cluster parameter configurations

``` bash
export HC_USER_CONFIG_DIR=/path-to/user-hadoop-config-params
export SC_USER_CONFIG_DIR=/path-to/user-spark-config-params
```

## Configure the base framework environment

**Without a dedicated separate master host node**

``` bash
source $DCF_HOME_DIR/sbin/dcf-configure.sh -d $DCF_HOME_DIR -c $DCF_CONF_DIR -s $DCF_SCRATCH_DIR -m 'no'
```

**With a dedicated separate master host node**

``` bash
source $DCF_HOME_DIR/sbin/dcf-configure.sh -d $DCF_HOME_DIR -c $DCF_CONF_DIR -s $DCF_SCRATCH_DIR -m 'yes'
```

## Configure a Hadoop cluster

First configure the cluster

``` bash
source $DCF_HOME_DIR/sbin/hadoop-cluster-configure.sh -f 'yes' -u $HC_USER_CONFIG_DIR
```

Format the namenode and fire up all daemons

``` bash
$HADOOP_HOME/bin/hdfs namenode -format
$HADOOP_HOME/sbin/start-all.sh
```

The status of HDFS can be checked with, e.g.,:

``` bash
hdfs dfsadmin -report
```

*Wordcount example*

Create a directory on the HDFS partition, put some text file in it, and run the application:

``` bash
hdfs dfs -mkdir /data
hdfs dfs -put $HADOOP_HOME/README.txt /data

export HADOOP_VERSION=3.2.1

hadoop jar $HADOOP_HOME/share/hadoop/mapreduce/hadoop-mapreduce-examples-${HADOOP_VERSION}.jar wordcount /data/README.txt /data/wordcount-out
```

or if using Yarn

``` bash
yarn jar $HADOOP_HOME/share/hadoop/mapreduce/hadoop-mapreduce-examples-${HADOOP_VERSION}.jar wordcount /data/README.txt /data/wordcount-out
```

*Pi calculation*

``` bash
hadoop jar $HADOOP_HOME/share/hadoop/mapreduce/hadoop-mapreduce-examples-${HADOOP_VERSION}.jar pi 16 1000
```

or with Yarn

``` bash
yarn jar $HADOOP_HOME/share/hadoop/mapreduce/hadoop-mapreduce-examples-${HADOOP_VERSION}.jar pi 16 1000
```

**Web UI**

Create a tunnel to the namenode host (e.g., on port 8088) and in a browser with access to the HPC cluster (e.g., one running on it) navigate to http://<namenode_number>:8088

## Configure a Spark cluster

First configure the cluster

``` bash
source $DCF_HOME_DIR/sbin/spark-cluster-configure.sh -u $SC_USER_CONFIG_DIR
```

or if using Yarn (configure Hadoop and start Yarn first, using HDFS is optional)

``` bash
source $DCF_HOME_DIR/sbin/spark-cluster-configure.sh -u $SC_USER_CONFIG_DIR -y 'yes'
```

Fire up all Spark daemons with 5 worker instances per slave host

``` bash
source $DCF_HOME_DIR/sbin/start-spark-cluster.sh -i 5
```

Then, e.g., start pyspark in standalone mode with

``` bash
pyspark --master spark://<MASTER_HOST>:<PORT>
```

Stop all Spark services with

``` bash
source $DCF_HOME_DIR/sbin/stop-spark-cluster.sh
```

*Example Spark applications*

There are few simple examples on the Apache Spark official page here:

https://spark.apache.org/docs/latest/submitting-applications.html

``` bash
export SPARK_VERSION=3.0.0
```

Pi calculation example over Yarn - with regards to the default example user cluster settings - in deploy mode master:

``` bash
$SPARK_HOME/bin/./spark-submit --class org.apache.spark.examples.SparkPi --master yarn --deploy-mode cluster --executor-memory 14G --executor-cores 4 --num-executors 39 $SPARK_HOME/examples/jars/spark-examples_2.12-${SPARK_VERSION}.jar 500
```

or in deploy mode client:

``` bash
$SPARK_HOME/bin/./spark-submit --class org.apache.spark.examples.SparkPi --master yarn --deploy-mode client --executor-memory 14G --executor-cores 4 --num-executors 39 $SPARK_HOME/examples/jars/spark-examples_2.12-${SPARK_VERSION}.jar 500
```

**Web UI**

Similarly, via a tunnel navigate, e.g., to http://<master_node>:8080 (default port) to access the Spark master.
The running jobs can be viewed at  http://<master_node>:4040 (default port).

If submitting over Hadoop then the user can also navigate to the Hadoop cluster Web UI and examine the submitted Spark applications from there.

## Stop all processes and reset DCF

An example sequence to stop all processes and reconfigure DCF for a new run if, e.g., Spark over Yarn has been set up

``` bash
source $DCF_HOME_DIR/sbin/stop-spark-cluster.sh
$HADOOP_HOME/sbin/stop-yarn.sh
$HADOOP_HOME/sbin/stop-dfs.sh
```

Then configure DCF anew as explained above. Any previous DCF directories (configuration, scratch, etc...) if existing with exactly the same names and notations (e.g., if rerunning DCF within the same batch job) will be backed up.
